#!/bin/bash

# loading configuration
if [ -f ~/.terrax/axe ]; then
	source ~/.terrax/axe
fi

# defining terrax release
release="3.0.1"

# defining base 3rd party software versions
awsvault="6.3.1"
chamber="2.10.7"
tfsec="0.63.1" # https://github.com/aquasecurity/tfsec/issues/1188
terraform=(\
"1.1.2" \
"1.4.5" \
)

# defining passphrase
passphrase=$(openssl rand -base64 17)

# defining pre-requisites
prerequisite=(\
"curl" \
"unzip" \
)

# defining product environments
environment=(\
"dev" \
"pre" \
"pro" \
)

# defining terrax mesages
declare -A messagecode=(\
["err00"]="terrax-error: im not able to target an objetive, workspace_key_prefix definition does not exist" \
["err01"]="terrax-error: there is nothing here that concerns me, terraform.tf does not exists" \
["err02"]="terrax-error: i have not no knowledge of this planet, run ./terrax install" \
["err03"]="terrax-error: i demand a valid version right now!" \
["err04"]="terrax-error: looks like im not the first herald you met, run terrax uninstall" \
["err05"]="terrax-error: you are not the true terrax master.. do not anger the worlds eater" \
["err06"]="terrax-error: are you trying to trick me?" \
["err07"]="terrax-error: is this the real world??, run terrax init" \
["inf00"]="terrax-info: im ready to seek..." \
["inf01"]="terrax-info: that sounds really insteresting, right?"
["inf02"]="terrax-info: there are no worlds alike in this universe but those ones... indeed looks pretty similar" \
["inf03"]="terrax-info: what have just happen?!, i feel like i have forgotten something important" \
["inf04"]="terrax-info: your regreat makes me stronger" \
["inf05"]="terrax-info: i feel myself revilatized, i'm ready to feed \"The Hunger\"" \
["inf06"]="terrax-info: i guess here is where our paths diverge..." \
["inf07"]="terrax-info: ok, it makes sense" \
["inf08"]="terrax-info: with great power comes a big axe... or something like this" \
["inf09"]="terrax-info: there is nothing here, shall i do something?" \
)

# landing function
landing () {
	if [[ "${terrax}" = "true" ]]; then
		echo -en "AWS Region: "
		read region
		echo -n "SSO url: "
		read sso_start_url
		echo -n "SSO account: "
		read sso_account_id
	    echo -n "SSO role: "
		read sso_role_name

		# defining sso profile
		cat > ${AWS_CONFIG_FILE} <<- EOM
[profile master]
region=${region}
sso_region=${region}
sso_start_url=${sso_start_url}
sso_account_id=${sso_account_id}
sso_role_name=${sso_role_name}
EOM
		return 0
 	else
		echo -e "\n${messagecode[err02]}"
		return 1
	fi
}

# seeking function
seeking () {
	if [[ "${terrax}" = "true" ]]; then
		if [ -f terraform.tf ]; then
			# obtaining product name from terraform.tf this is required to use chamber properly
			product=$(grep "workspace_key_prefix" terraform.tf | awk '{print $3}' | awk -F "\"" '{print $2}')
			if [ ${product} ]; then
				if [ -f .terraform/environment ]; then
					workspace=$(cat .terraform/environment)
					return 0
				else
					if [[ "$1" == "init" ]] ;then
						return 0
					else				
						echo -e "\n${messagecode[err07]}"
						return 1
					fi
				fi
			else
				echo -e "\n${messagecode[err00]}"
				return 1
			fi
		else
			echo -e "\n${messagecode[err01]}"
			return 1
		fi
	else
		echo -e "\n${messagecode[err02]}"
		return 1
	fi
}

# defining cases
if [ $EUID -ne 0 ]; then
	case "$1:$2:$3" in

		# tfsec
		check:${null}:${null})
			seeking
			if [ $? -eq 0 ]; then
				exception=$(aws-vault exec master -- chamber read ${product}/${workspace} tfsec_exception 2>/dev/null | grep "tfsec_exception"| awk '{ print $2 }' | sed 's/#/,/g')		
				# checking if there are any exception
				if [[ ${exception} ]]; then
					tfsec -e ${exception}
				else
					tfsec
				fi
			else
				exit 1
			fi
		;;

		# tfsec - exception list
		check:exception:${null})
			seeking
			if [ $? -eq 0 ]; then
				# checking if there are any exception
				exception=$(aws-vault exec master -- chamber read ${product}/${workspace} tfsec_exception 2>/dev/null | grep "tfsec_exception" | awk '{ print $2 }' | sed 's/#/\n/g' | sort)
				if [[ ${exception} ]]; then
					echo -e "${product}/${workspace}...........................................\n"
					echo "${exception}"
					echo -e "\n${messagecode[inf08]}"
				else
					echo -e "\n${messagecode[inf09]}"
				fi
			else
				exit 1
			fi
		;;

		# tfsec - add exception
		check:exception:add)
			seeking
			if [ $? -eq 0 ]; then
				addexception=$(echo "$4" | tr '[:upper:]' '[:lower:]')
				currentexception=$(aws-vault exec master -- chamber read ${product}/${workspace} tfsec_exception 2>/dev/null | grep -v "Key" | awk '{ print $2 }')

				# checking if there are any exception
				if [ ${currentexception} ]; then
					if ! [[ ${currentexception} =~ (^|"#")"${addexception}"($|"#") ]] ; then
						aws-vault exec master -- chamber write ${product}/${workspace} tfsec_exception "${addexception}#${currentexception}"
						echo -e "\n${messagecode[inf07]}"
					else 
						echo -e "\n${messagecode[err06]}"
						exit 1
					fi
				else
					# no exceptions or even no secret defined
					aws-vault exec master -- chamber write ${product}/${workspace} tfsec_exception "${addexception}"
					echo -e "\n${messagecode[inf07]}"
				fi
			else
				exit 1
			fi
		;;

		# tfsec - remove exception
		check:exception:remove)
			seeking
			if [ $? -eq 0 ]; then
				removeexception=$(echo "$4" | tr '[:upper:]' '[:lower:]')
				currentexception=$(aws-vault exec master -- chamber read ${product}/${workspace} tfsec_exception 2>/dev/null | grep -v "Key" | awk '{ print $2 }')

				# checking if there were any exception already
				if [ ${currentexception} ]; then
					if [[ ${currentexception} =~ (^|"#")"${removeexception}"($|"#") ]]; then
						exception=$(echo "${currentexception}" | sed 's/\(\b'${removeexception}'#\b\)\|\(\b#'${removeexception}'\b\)//g')
						countexception=$(echo "${currentexception}" | sed 's/#/\n/g' | wc -l)
						# last exception
						if [ ${countexception} -eq 1 ]; then
							aws-vault exec master -- chamber delete ${product}/${workspace} tfsec_exception
						else 
							aws-vault exec master -- chamber write ${product}/${workspace} tfsec_exception ${exception}
						fi
						echo -e "\n${messagecode[inf07]}"
					else 
						echo -e "\n${messagecode[err06]}"
						exit 1
					fi
				else
					# no exceptions or even no secret defined
					echo -e "\n${messagecode[err06]}"
				fi
			else
				exit 1
			fi
		;;

		# aws-vault - status
		config:${null}:${null})
			if [[ "${terrax}" = "true" ]]; then
				echo -e "awsvault/$1...........................................\n"
				grep -i "sso" ${AWS_CONFIG_FILE}
				aws-vault exec master --no-session -- env | grep -e "AWS_REGION" -e "AWS_ACCESS_KEY_ID" -e "AWS_SECRET_ACCESS_KEY"
				echo -e "\n${messagecode[inf00]}"
 			else
				echo -e "\n${messagecode[err02]}"
				exit 1
			fi
		;;
	
		# aws-vault - configure sso
		config:profile:${null})
			landing
			if [ $? -eq 0 ]; then
				echo -e "\n${messagecode[inf00]}"
			else
				exit 1
			fi
		;;

		# terraform - configure version
		config:version:${null})
			if [[ "${terrax}" = "true" ]]; then
				echo "terraform/${version}..........................................."
				echo -e "\nAllowed terraform versions:"
				ls -1  ~/.terrax/terraform_versions/
				echo -en "\nEnter the required version: "
				read answer
				if [ ${answer} ] && printf '%s\n' "${terraform[@]}" | grep -xq ${answer}; then
					sed -i "s/${version}/${answer}/g" ~/.terrax/axe
					source ~/.terrax/axe
					ln -sfn ~/.terrax/terraform_versions/${version}/terraform ~/.terrax/terraform
					echo -e "\n${messagecode[inf00]}"
				else
					echo -e "\n${messagecode[err03]}"
					exit 1
				fi
			else
				echo -e "\n${messagecode[err02]}"
				exit 1
			fi
		;;

		# chamber - secret list
		secret:${null}:${null})
			seeking
			if [ $? -eq 0 ]; then
				secret=$(aws-vault exec master -- chamber list -e ${product}/${workspace}  | grep -wv "tfsec_exception" | grep -v "Key" | awk '{ print (tolower(substr($1,8))" "$6)}')
				# checking if there is any secret
				if [[ ${secret} ]]; then
					echo -e "${product}/${workspace}...........................................\n"
					echo "${secret}" | column -t
					echo -e "\n${messagecode[inf08]}"
				else
					echo -e "\n${messagecode[inf09]}"
				fi
			else
				exit 1
			fi
		;;

		# chamber - add secret
		secret:add:*)
			seeking
			if [ $? -eq 0 ]; then
				addsecret=$(echo "tf_var_$3" | tr '[:upper:]' '[:lower:]')
				echo "$4" | sed -e 's/[^a-zA-Z 0-9]/\\&/g' | xargs aws-vault exec master -- chamber write ${product}/${workspace} ${addsecret}
				echo -e "\n${messagecode[inf01]}"
			else
				exit 1
			fi
		;;

		# chamber - remove secret
		secret:remove:*)
			echo -n "are u sure mortal? this action cannot be undone (y/n)?"
			read answer
			if [[ "$answer" != "${answer#[Yy]}" ]] ;then
				seeking
				if [ $? -eq 0 ]; then
					removesecret=$(echo "tf_var_$3" | tr '[:upper:]' '[:lower:]')
					aws-vault exec master -- chamber delete ${product}/${workspace} ${removesecret}
					sleep 1
					echo -e "\n${messagecode[inf03]}"
				else
					exit 1
				fi
			else
				echo -e "\n${messagecode[inf04]}"
			fi
		;;

		# terraform - init mock
		init:*:${null})
			seeking $1
			if [ $? -eq 0 ]; then
				# terraform init and --reconfigure support 0.12 > 0.13
				aws-vault exec master -- terraform init $2
				
				# defining workspaces
				workspacelist=$(aws-vault exec master -- terraform workspace list | grep "[aA-zZ]" | wc -l)
				if [ ${workspacelist} -eq 1 ]; then
					# creating environment workspaces
					for n in "${environment[@]}"
					do
						aws-vault exec master -- terraform workspace new ${n}
					done
					aws-vault exec master -- terraform workspace select ${environment[0]}
				else
					if ! [ -f .terraform/environment ]; then
						aws-vault exec master -- terraform workspace select ${environment[0]}
					fi
				fi
				echo -e "\n${messagecode[inf05]}"
			else
				exit 1
			fi
		;;

		# help and command list
		help:${null}:${null})
			echo "terrax v${release} help............................................"
			echo " + check                         (tfsec check)"
			echo " |-> + check exception           (list tfsec exception)"
			echo "     |-> check exception add     (add tfsec exception)"
			echo "     |-> check exception remove  (remove tfsec exception)"
			echo ".............................................................."
			echo " + config                        (aws-vault config status)"
			echo " |-> config profile              (configure aws-vault profile)"
			echo " |-> config version              (configure terraform version)"
			echo ".............................................................."
			echo " + secret                        (list chamber secrets)"
			echo " |-> secret add                  (add chamber secret)"
			echo " |-> secret remove               (remove chamber secret)"
			echo ".............................................................."
			echo " + init                          (terraform init mock)"
			echo " + install                       (terrax install)"
			echo " + uninstall                     (terrax uninstall)"
			echo ".............................................................."
			echo " !!!*whateverelse* will be forwarded to terraform binary"
			echo ".............................................................."
		;;

		# install
		install:${null}:${null})
			if [ ! ${terrax} ] && [ ! -d .terrax ]; then

				# prerequisite
				echo "[1/9]... checking required pre-installed software"
				for n in "${prerequisite[@]}"
				do
					precheck=$(dpkg -s ${n}  2>/dev/null | grep "Status" | grep -c "installed")
					if [ ${precheck} -eq 0 ]; then
						echo -e "\nplease install ${n} and try installing terrax again..."
						exit 1
					fi
				done

				# defining local user paths
				mkdir ~/.terrax
				mkdir ~/.terrax/terraform_versions

				# cool stuff
				echo "[2/9]... downloading aws-vault version ${awsvault}"
				curl -sL https://github.com/99designs/aws-vault/releases/download/v${awsvault}/aws-vault-linux-amd64 --output ~/.terrax/aws-vault
				echo "[3/9]... downloading chamber version ${chamber}"
				curl -sL https://github.com/segmentio/chamber/releases/download/v${chamber}/chamber-v${chamber}-linux-amd64 --output ~/.terrax/chamber
				echo "[4/9]... downloading tfsec version ${tfsec}"
				curl -sL  https://github.com/aquasecurity/tfsec/releases/download/v${tfsec}/tfsec-linux-amd64 --output ~/.terrax/tfsec
				echo "[5/9]... downloading terraform version(s) ${terraform[*]}"
				for n in "${terraform[@]}"
				do
					curl -sL https://releases.hashicorp.com/terraform/${n}/terraform_${n}_linux_amd64.zip --output ~/.terrax/terraform_${n}.zip
					unzip ~/.terrax/terraform_${n}.zip -d ~/.terrax/terraform_versions/${n} > /dev/null
					rm -rf ~/.terrax/terraform_${n}.zip
				done
				chmod +x ~/.terrax/*
				echo "[6/9]... sharpening the axe"
				here=$(pwd) && cat > ~/.terrax/axe <<- EOM
#config########################
export terrax="true"
export version="$(echo "${terraform[*]}" | sed 's/ /\n/g' | sort -n | head -1)"
export PATH="$PATH:$HOME/.terrax/"
#chamber#######################
export CHAMBER_KMS_KEY_ALIAS="aws/ssm"
#aws-vault#####################
export AWS_VAULT_BACKEND="file"
export AWS_ASSUME_ROLE_TTL="8h"
export AWS_SESSION_TOKEN_TTL="8h"
export AWS_CONFIG_FILE="$HOME/.terrax/sso"
export AWS_VAULT_FILE_PASSPHRASE="${passphrase}"
###############################
EOM
				echo "[7/9]... creating terrax simbolic link (sudo is required)"
				sudo ln -s ${here}/terrax /usr/local/bin
				echo "[8/9]... loading custom env variables"
				source ~/.terrax/axe
				ln -s ~/.terrax/terraform_versions/${version}/terraform ~/.terrax/terraform
				echo "[9/9]... defining aws-vault sso profile"
				landing
				if [ $? -eq 0 ]; then
					echo -e "\n${messagecode[inf00]}"
				else
					exit 1
				fi
			else
				echo -e "\n${messagecode[err04]}"
			fi
		;;

		# uninstall
		uninstall:${null}:${null})
			echo -n "are u sure? this action cannot be undone (y/n)?"
			read answer
			if [[ "${answer}" != "${answer#[Yy]}" ]] ;then
				echo "[1/3]... removing terrax installation"
				find / -name ".terrax*" -exec rm -rf {} \; 2>/dev/null
				echo "[2/3]... removing aws-vault secrets"
				rm -rf ~/.awsvault
				echo "[3/3]... removing simbolic link (sudo is required)"
				sudo unlink /usr/local/bin/terrax  > /dev/null 2>/dev/null
				unset terrax
				echo -e "\n${messagecode[inf06]}"
			else
				echo -e "\n${messagecode[inf04]}"
			fi
		;;

		# whateverelse
		*)
			seeking
			if [ $? -eq 0 ]; then
				aws-vault exec master -- chamber exec ${product}/${workspace} -- terraform $*
			else
				exit 1
			fi
		;;

	esac
else
	echo -e "\n${messagecode[err05]}"
fi

exit 0

#whoami###############################                                
# https://en.wikipedia.org/wiki/Terrax
# scripted by Alvaro Fernandez Martin
######################################