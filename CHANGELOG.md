# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- tfsec custom checks

## [v3.1.0] - 24/04/2023

### added

- terraform v1.4.5

### deleted 

- terraform v1.0.2

## [v3.0.1] - 04/01/2022

### fixed

- new products terraform init

### added

## [v3.0.0] - 21/12/2021

### added

- aws sso support **(NOT backward compatible)**
- tfsec product support **(NOT backward compatible)**

### changed

- upgraded to terraform 1.1.2
- upgraded to chamber 2.10.7
- upgraded to tfsec 0.63.1 **(NOT backward compatible)**
- terraform init mock
- improved secret management
- imporved exceptions management

### removed

- terraform 0.14.10 support
- aws-vault programatic access
- aws-vault programatic keys rotation
- chamber secret comparation

### fixed

- help
- README.md

## [v2.1.0] - 15/07/2021

### added

- pre-requisites check

### changed

- upgraded to terraform 1.0.2
- upgraded to chamber 2.10.1
- upgraded to tfsec 0.48.2

## [v2.0.0] - 22/04/2021

### added

- tfsec 0.39.20
- enhanced secrets management
- enhanced exceptions management
- access and secret keys rotation

### changed

- upgraded to terraform 0.14.10
- upgraded to aws-vault 6.3.1
- upgraded to chamber 2.9.1

### removed

- terraform 0.13.4 support

## [v1.4.0] - 22/10/2020

### removed

- terraform 0.12.28 support

### added

- init persistance
- init workspace(s) definition

### changed

- upgraded to terraform 0.13.4
- upgraded to aws-vault 6.2.0
- upgraded to chamber 2.8.2

## [v1.3.0] - 29/07/2020

### added

- Windows 10 support @jorge.sanmartin

### fixed

- preview at terraform 0.12.28
- terrax axe path definition @jorge.sanmartin

## [v1.2.0] - 01/07/2020

### added

- terraform 0.12.28 support
- allowed to select between terraform 0.12.0 and 0.12.28

### removed

- terraform 0.12.20 support

## [v1.1.0] - 26/06/2020

### changed

- upgraded to terraform 0.12.20

## [v1.0.0] - 31/03/2020

### changed

- project variable obtained from terraform.tf **(NOT backward compatible)**
- enhanced preview command
- enhanced list command
- README.md

### added

- escaped chamber key value

## [v0.1.0] - 19/03/2020

### added

- moved to awsvault 5.3.2
- moved to chamber 2.8.0
- MFA support